const http = require("http");
const { v4: uuidv4 } = require("uuid");

const html = `<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
      <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
      <p> - Martin Fowler</p>

  </body>
</html>`;git 

const json = `{
    "slideshow": {
      "author": "Yours Truly",
      "date": "date of publication",
      "slides": [
        {
          "title": "Wake up to WonderWidgets!",
          "type": "all"
        },
        {
          "items": [
            "Why <em>WonderWidgets</em> are great",
            "Who <em>buys</em> WonderWidgets"
          ],
          "title": "Overview",
          "type": "all"
        }
      ],                                                                                                                                                 
      "title": "Sample Slide Show"
    }
  }`;

const server = http.createServer((request, response) => {
  try {
    const { url, method } = request;

    if (url == "/html" && method == "GET") {
      response.writeHead(200, { "content-type": "text/html" });
      response.write(html);
      response.end();
    } else if (url == "/json" && method == "GET") {
      response.writeHead(200, { "content-type": "application/json" });
      response.end(json);
    } else if (url == "/uuid" && method == "GET") {
      response.writeHead(200, { "content-type": "application/json" });
      let obj = {
        uuid: uuidv4(),
      };
      response.end(JSON.stringify(obj));
    } else if (url.startsWith("/status") && method == "GET") {
      let status = url.split("/")[2];

      if (status) {
        response.writeHead(status);
        response.end(`${status} status code`);
      } else {
        response.writeHead(404);
        response.end(`Expected status code in the url params`);
      }
    } else if (url.startsWith("/delay") && method == "GET") {
      let delay = url.split("/")[2];

      if (delay) {
        setTimeout(() => {
          response.writeHead(200);
          response.end(`Delayed for ${delay} seconds`);
        }, delay * 1000);
      } else {
        response.writeHead(404);
        response.end(`Expected delay in the url params`);
      }
    } else if (url == "/tasks" && method == "GET") {
      // let foo = async () => {
      //   const data = await fetch(
      //     "https://taskee-backend.onrender.com/api/task/get-all-tasks",
      //     {
      //       method: "GET",
      //       headers: {
      //         "content-type": "application/json",
      //       },
      //     }
      //   );
      //   const res = await data.json();
      //   response.end(JSON.stringify(res));
      // };

      // foo();
      fetch("https://taskee-backend.onrender.com/api/task/get-all-tasks", {
        method: "GET",
        headers: {
          "content-type": "application/json",
        },
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          response.end(JSON.stringify(data));
        });

    } else {
      response.writeHead(404);
      response.write("Page Not Found");
      response.end();
    }
  } catch (error) {
    console.log(error, "Internal Server error");
    response.writeHead(500);
    response.end();
  }
});

server.listen(3000, () => {
  console.log("Server running on port 3000");
});
